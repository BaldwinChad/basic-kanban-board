# basic-kanban-board

## How to Run

Run `npm install` once that is finished run `npm run serve` and navigate to `localhost:8080` in any web browser.

## A Trello knockoff kanban board

Build a Simple Trello Board knockoff using either Vue, Angular or React (your choice)
Board should have 3 hard coded columns for holding “cards”

1. TODO
1. IN-PROGRESS
1. DONE

The “cards” can be very basic only capturing a short description of what needs to done. (Feel free to add anything else that you think makes it better)
There will need to be an “add” button someplace for adding new “cards”

1. This add button should present the user a way to enter the required field(s), with the end result being a new card in the TODO column.

The user should be able to move a card between each column (how that is done is up to you)

## (Optional)

Build a backend API (probably something semi-RESTful) to manage the state of the board in some sort of persistent storage. This storage could be a database (CSVs, MySQL, Postgres, Mongo, something I haven’t heard of), and the API can be written in whatever stack you prefer.  Examples would be Node.js (with express), .Net Core, Java with Spring Boot, something else that you feel will allow you to build it fast.

