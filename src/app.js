angular.module('kanban', ['ngDragDrop'])
  /**
   * Directive for each column on the kanban board
   **/
  .directive('cardColumn', () => ({
    restrict: 'E',
    scope: {
      columnItems: '=items',
      columnTitle: '@title',
    },
    link: (scope) => {
      const curScope = scope;
      /*
       * Text that will be used for the description
       * of a new card in the column
       */
      curScope.newCardText = '';

      /**
       * Takes the text out of column text input, creates
       * a new card and adds it to the column.
       * @date 2020-06-08
       */
      curScope.addCard = () => {
        curScope.columnItems.push({
          description: curScope.newCardText,
        });

        // reset the input model
        curScope.newCardText = '';
      };

      /**
       * Moves a card in the column by the numberOfSpaces
       * @date 2020-06-08
       * @param {Number} itemIndex the index of the card in columnItems
       * @param {Number} numberOfSpaces the number of spaces to move the item, negatives move the item up
       */
      curScope.move = (itemIndex, numberOfSpaces) => {
        const tempCard = curScope.columnItems[itemIndex + numberOfSpaces];

        curScope.columnItems[itemIndex + numberOfSpaces] = curScope.columnItems[itemIndex];
        curScope.columnItems[itemIndex] = tempCard;
      };


      /**
       * Returns if the item can be moved up the column list
       * @date 2020-06-08
       * @param {Number} itemIndex the index of the card in columnItems
       * @returns {Boolean} if the item can move up
       */
      curScope.canMoveUp = (itemIndex) => itemIndex > 0 && curScope.columnItems.length > 1;

      /**
       * Returns if the item can be moved down the column list
       * @date 2020-06-08
       * @param {Number} itemIndex the index of the card in columnItems
       * @returns {Boolean} if the item can move down
       */
      curScope.canMoveDown = (itemIndex) => itemIndex < curScope.columnItems.length - 1
        && curScope.columnItems.length > 1;
    },
    templateUrl: 'src/templates/column.html',
  }))
  .controller('cardCtrl', ($scope) => {
    /*
     * Quick arrays for each of the columns
     */
    $scope.todoItems = [{
      description: 'This is an example card',
    }];
    $scope.inProgressItems = [];
    $scope.doneItems = [];
  });
